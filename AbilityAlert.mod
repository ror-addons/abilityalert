<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<UiMod name="AbilityAlert" version="2.9.2" date="10/02/2008" >
	
		<Author name="uce_mike" email="uce_mike@yahoo.com" />
	
		<Description text="Visual alerts for abilities cooldown." />
<VersionSettings gameVersion="1.4.5" windowsVersion="1.0" savedVariablesVersion="1.0" />

        <Dependencies>
            <Dependency name="LibSlash" />
            <Dependency name="EA_ActionBars" />
        </Dependencies>

		<SavedVariables>
		  <SavedVariable name="AbilityAlertFilterOptions" />
		</SavedVariables>

		<Files>
			<File name="AbilityAlert.lua" />
			<File name="AbilityAlert.xml" />
		</Files>

		<OnInitialize>
			<CallFunction name="AbilityAlert.Initialize" />
		</OnInitialize>

		<OnShutdown>
			<CallFunction name="AbilityAlert.Shutdown" />
		</OnShutdown>

	</UiMod>

</ModuleFile>    