--[[
 
    Sends a visual alert when a ability cooldown is up and ready for use.
    
    if you have WSCT installed it can send the alert to WSCT instead of the built in display
    
    uce_mike@yahoo.com
 
--]]

AbilityAlert = {}
AbilityAlert.hooked = {}
AbilityAlert.hooked.ActionButton_UpdateCooldownAnimation = nil
AbilityAlert.hooked.ActionButton_UpdateEnabledState = nil

local AA_MIN_COOLDOWN   = 2     --GCD is 1.6 seconds, if it recasts that fast we don't care.


local function print(txt)
    TextLogAddEntry("Chat", 0, towstring(txt))
end

--- clean up events
function AbilityAlert.Shutdown()
	UnRegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "AbilityAlert.CombatEvent")
	--UnRegisterEventHandler(SystemData.Events.PLAYER_COOLDOWN_TIMER_SET, "AbilityAlert.TestCOOLDOWN")
end

--- register events, set vars, initialize function replacement
function AbilityAlert.Initialize()
    -- force debug window to not shrink up, why they changed this is beyond me
    LogDisplaySetEntryLimit("DebugWindowText", 5000)
    --end
    
	CreateWindow("AAWindow", true)
	LayoutEditor.RegisterWindow("AAWindow", L"Ability Alert!", L"Alerts when cooldown completed and combat events.", false, false, true, nil)
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "AbilityAlert.CombatEvent")
	--RegisterEventHandler(SystemData.Events.PLAYER_COOLDOWN_TIMER_SET, "AbilityAlert.TestCOOLDOWN")

    -- savedvariables
    if not AbilityAlertFilterOptions then
        AbilityAlertFilterOptions = {
            ExcludedAbilities = {},
            isWCTDisabled = false,
            WSCTFrame = 3,
            useBoth = false,
            showReactive = false,
            showAll = true,
            ReactiveAbilities = {L"shield of reprisal",},
            AA_Version = "2.9.2",
        }
    end
    -- new vars need to set
    if not AbilityAlertFilterOptions.ReactiveAbilities then
        AbilityAlertFilterOptions.ReactiveAbilities = {L"shield of reprisal",}
    end
    if not AbilityAlertFilterOptions.AA_Version or 
            AbilityAlertFilterOptions.AA_Version ~= "2.8" then
        AbilityAlertFilterOptions.showAll = true
        AbilityAlertFilterOptions.WSCTFrame = 3
        AbilityAlertFilterOptions.AA_Version = "2.8"
    end
    -- end new vars
    
   if not LibSlash.IsSlashCmdRegistered("aa") then
       LibSlash.RegisterWSlashCmd("aa", function(args) AbilityAlert.SlashHandler(args) end)
       LibSlash.RegisterWSlashCmd("abilityalert", function(args) AbilityAlert.SlashHandler(args) end)
    else      
       print("Warning: something else seems to be using /aa - AbilityAlert won't be able to. Use /abilityalert instead.")
       LibSlash.RegisterWSlashCmd("abilityalert", function(args) AbilityAlert.SlashHandler(args) end)
    end
    
---------------------------- [replace function ActionButton:UpdateCooldownAnimation (self, timeElapsed, updateCooldown) ] -----------------------------
-- Perhaps when they actually fix the PLAYER_COOLDOWN_TIMER_SET event that's triggered 
-- I can stop using this.
AbilityAlert.hooked.ActionButton_UpdateCooldownAnimation = ActionButton.UpdateCooldownAnimation 
ActionButton.UpdateCooldownAnimation = AbilityAlert.ActionButton_UpdateCooldownAnimation
--- used to get abilities that trigger on block/parry
AbilityAlert.hooked.ActionButton_UpdateEnabledState = ActionButton.UpdateEnabledState
ActionButton.UpdateEnabledState = AbilityAlert.ActionButton_UpdateEnabledState
---------[end replacement function]-----------

        if WSCT then
            TextLogAddEntry("Chat",0,L"Ability Alert!: Loaded, using WSCT output!");
        else
            TextLogAddEntry("Chat",0,L"Ability Alert!: Loaded!");
        end
end

-- this stuff is being tested, only shows up with showReactive is enabled
-- this alerts when a ability is now ready (block/parry trigger abilities?)
function AbilityAlert.ActionButton_UpdateEnabledState(self, isSlotEnabled, isTargetValid,...)
    local isThisSlotEnabled = isSlotEnabled
    local isThisTargetValid = isTargetValid
    
    AbilityAlert.hooked.ActionButton_UpdateEnabledState (self, isSlotEnabled, isTargetValid,...)

    local isThisSlotEnabledAfter = isSlotEnabled
    local isThisTargetValidAfter = isTargetValid
    
    local abilityData = GetAbilityData (self.m_ActionId)
--    if AbilityAlertFilterOptions.showReactive and AbilityAlert.isReactiveMonitored(abilityData.name)then
        if AbilityAlert.isReactiveMonitored(abilityData.name)then
            if ( (isThisSlotEnabled and isThisSlotEnabledAfter) ) then  
--                 and (isThisTargetValid and isThisTargetValidAfter) ) then
                AbilityAlert.Print(self.m_ActionId, (AA_MIN_COOLDOWN+1))
            end
        end

end

--- hook into the original and peak at these values since PLAYER_COOLDOWN_TIMER_SET does nothing.
function AbilityAlert.ActionButton_UpdateCooldownAnimation(self, timeElapsed, updateCooldown,...)
    local COOLDOWN          = 2
    local COOLDOWN_TIMER    = 3
    
    local updateCooldown    = updateCooldown or self.m_RequiresFullUpdate

    local myMaxCooldown = self.m_MaxCooldown -- store MaxCooldown
    -- run the core function
    AbilityAlert.hooked.ActionButton_UpdateCooldownAnimation(self, timeElapsed, updateCooldown,...)

    local myMaxCooldownAfter = self.m_MaxCooldown -- store "after" current MaxCooldown
    local myCurrentCooldownAfter = self.m_Cooldown
    local abilityData = GetAbilityData (self.m_ActionId)

    if (updateCooldown) then
        if (myMaxCooldown > AA_MIN_COOLDOWN and myMaxCooldownAfter < AA_MIN_COOLDOWN ) then
            -- alert that the ability is ready
            if (not AbilityAlert.isReactiveMonitored(abilityData.name)) then       
                AbilityAlert.Print(self.m_ActionId, myMaxCooldown)
            end
        end
    elseif (myCurrentCooldownAfter <= 0) then
            -- alert that the ability is ready
            if (not AbilityAlert.isReactiveMonitored(abilityData.name)) then       
                AbilityAlert.Print(self.m_ActionId, self.m_MaxCooldown)
            end
    end

end
---------------

-- for testing, not used right now
function AbilityAlert.TestCOOLDOWN(sTest1, sTest2, sTest3, sTest4)
print("-AA: AbilityAlert.TestCOOLDOWN()")    
    DUMP_TABLE(sTest1)
    DUMP_TABLE(sTest2)
    DUMP_TABLE(sTest3)
    DUMP_TABLE(sTest4)
end

-- combat event occured, check for parry/block
function AbilityAlert.CombatEvent(targetObjNum , eventMagnitude , eventType )
    -- myTarget
	if targetObjNum  == GameData.Player.worldObjNum and 
        eventType == GameData.CombatEvent.PARRY then
        --- parried
        if ((AbilityAlertFilterOptions.showAll and not AbilityAlert.isFiltered("parry")) or    
            (not AbilityAlertFilterOptions.showAll and AbilityAlert.isFiltered("parry")) ) then        
            AbilityAlert.Display("[PARRY]")
        end
        
    -- myTarget
   	elseif targetObjNum  == GameData.Player.worldObjNum and 
            eventType == GameData.CombatEvent.BLOCK then
        --- blocked
        if ((AbilityAlertFilterOptions.showAll and not AbilityAlert.isFiltered("block")) or    
            (not AbilityAlertFilterOptions.showAll and AbilityAlert.isFiltered("block")) ) then        
            AbilityAlert.Display("[BLOCK]")
        end
	else
	-- notta
	end

end

-- check timer on CD, if valid pass to the Display function
function AbilityAlert.Print(ActionID,MaxCooldown)
    if (MaxCooldown > AA_MIN_COOLDOWN) then 
        local abilityData = GetAbilityData (ActionID)
        -- showall = true we ignore filterd, showall=false we show filtered only
        if ((AbilityAlertFilterOptions.showAll and not AbilityAlert.isFiltered(abilityData.name)) or
            (not AbilityAlertFilterOptions.showAll and AbilityAlert.isFiltered(abilityData.name)) ) then
            AbilityAlert.Display(abilityData.name)
        end
    end

end

--- actually display it
function AbilityAlert.Display(thisText)
    local msg = L"+"..towstring(thisText)
    
            -- send to wsct if using it
            if (WSCT and not AbilityAlertFilterOptions.isWCTDisabled) or 
                (WSCT and AbilityAlertFilterOptions.useBoth)then
                AbilityAlert.WSCTPrint(msg)
            end

            -- send to build in alert window otherwise
            if (AbilityAlertFilterOptions.useBoth or not WSCT or AbilityAlertFilterOptions.isWCTDisabled) then
        	   LabelSetText("AAText", msg)
              WindowStartAlphaAnimation("AAWindow",Window.AnimationType.EASE_OUT, 1.0, 0.0, 2, false, 0, 0)
            end
end

-- WSCT print, ability is ready.
function AbilityAlert.WSCTPrint(abilityName)
    local colorWSCT = {r = 20, g = 230, b = 250}
    local isCritWSCT = true
    local etypeWSCT = nil
    local frameWSCT = WSCT.FRAME3
    local anitypeWSCT = nil
    local objectIDWSCT = nil
    local iconWSCT = nil
    local msgWSCT = abilityName
    
--    if WSCT then    
        if AbilityAlertFilterOptions.WSCTFrame == 1 then
            frameWSCT = WSCT.FRAME1
        elseif AbilityAlertFilterOptions.WSCTFrame == 2 then
            frameWSCT = WSCT.FRAME2
        elseif AbilityAlertFilterOptions.WSCTFrame == 3 then
            frameWSCT = WSCT.FRAME3
        else
            frameWSCT = WSCT.FRAME3
        end    
--    end

--WSCT.DisplayText(msg, color, iscrit, etype, frame, anitype, objectID, icon)
--msg - the string to display (already converted to wstring)
--color- the color table, ex. {r=0,g=0,b=255}
--iscrit - boolean for if it is a crit or not
--etype - should be nil
--frame - which WSCT frame (1,2,3)
--anitype - which animation type to use (1-8, or if nil will use the frame's default)
--objectID - the world obj ID if trying to attach, can be nil
--icon - should be nil, icons aren't supported yet
 WSCT:DisplayText(msgWSCT,
                 colorWSCT,
                 isCritWSCT,
                 etypeWSCT,
                 frameWSCT,
                 anitypeWSCT,
                 objectIDWSCT,
                 iconWSCT)


-- WSCT:DisplayMessage( msgWSCT, colorWSCT)
-- WSCT:Display_Event("SHOWSPELL", msgWSCT, isCritWSCT, objectIDWSCT)
end

-- list slash commands
function AbilityAlert.CommandOptions()
        TextLogAddEntry("Chat", 0, L"------------Ability Alert! "..towstring(AbilityAlertFilterOptions.AA_Version)..L"-----------------")
        print("Valid options for /abilityalert:")
        print("show [all|none] - show all/none abilities. Filtertext will hide (all) or display (none)")
        print("add [filtertext] - adds a filter for the given alert text (can be partial)")
        print("del [filtertext] - removes a filter for the given alert text")
        print("list - lists currently active text filters")
--        print("reactive [on|off] - turns on/off reactive monitors")
        print("radd [monitorfilter] - adds ability name to reactive monitor (can be partial)")
        print("rdel [monitorfilter] - removes ability from reactive monitor")
        print("rlist - lists currently active reactive monitor")
        print("wsct [enable|disable|both] - enable/disable WSCT output or use both standard/WSCT")
        print("wsctframe [1|2|3] - change WSCT output frame to 1, 2 or 3.")
-- add change font options
--  LabelSetFont("AAWindow","font_clear_large",20)

end

-- process slash commands
function AbilityAlert.SlashHandler(args)
    local opt, val = args:match(L"([a-z0-9]+)[ ]?(.*)")
    
    -- NO OPTION SPECIFIED
    if not opt then
        AbilityAlert.CommandOptions()        
        
    -- ADD FILTER TEXT
    elseif opt == L"add" then
        if val == L"" or not val then
            print("Usage: /abilityalert add [filtertext]")
        else
            table.insert(AbilityAlertFilterOptions.ExcludedAbilities, val:lower())
            if AbilityAlertFilterOptions.showAll then
                print(L"Added alert hide filter for '"..val:lower()..L"'.")
            else
                print(L"Added alert display filter for '"..val:lower()..L"'.")
            end
        end
    
    -- REMOVE FILTER TEXT    
    elseif opt == L"del" then
        if val == L"" or not val then
            print("Usage: /abilityalert del [filtertext]")
        else
            for k,v in ipairs(AbilityAlertFilterOptions.ExcludedAbilities) do
                if v == val:lower() then
                    table.remove(AbilityAlertFilterOptions.ExcludedAbilities, k)
                    print(L"Removed alert filter for '"..val:lower()..L"'.")
                end
            end
        end
    -- LIST FILTER TEXT
    elseif opt == L"list" then
        if AbilityAlertFilterOptions.showAll then
            print("Currently active alert hide filters:")
        else
            print("Currently active alert display filters:")
        end
        for k,v in ipairs(AbilityAlertFilterOptions.ExcludedAbilities) do
            print(towstring(k)..L": "..v)
        end
        
    -- ADD reactive monitor TEXT
    elseif opt == L"radd" then
        if val == L"" or not val then
            print("Usage: /abilityalert radd [monitor filter]")
        else
            table.insert(AbilityAlertFilterOptions.ReactiveAbilities, val:lower())
            print(L"Added monitor filter for '"..val:lower()..L"'.")
        end
    
    -- REMOVE reactive monitor TEXT    
    elseif opt == L"rdel" then
        if val == L"" or not val then
            print("Usage: /abilityalert rdel [monitor filter]")
        else
            for k,v in ipairs(AbilityAlertFilterOptions.ReactiveAbilities) do
                if v == val:lower() then
                    table.remove(AbilityAlertFilterOptions.ReactiveAbilities, k)
                    print(L"Removed monitor filter for '"..val:lower()..L"'.")
                end
            end
        end
    -- LIST reactive monitor TEXT
    elseif opt == L"rlist" then
        print("Currently active monitor filter:")
        for k,v in ipairs(AbilityAlertFilterOptions.ReactiveAbilities) do
            print(towstring(k)..L": "..v)
        end


    -- enable/disable WSCT support
    elseif opt == L"wsct" then
        if val == L"" or not val then
            print("Usage: /abilityalert wsct [enable|disable]")
        elseif (val:lower()) == L"disable" then
            AbilityAlertFilterOptions.useBoth = false
            AbilityAlertFilterOptions.isWCTDisabled = true
            print("WSCT output disabled.")
        elseif (val:lower()) == L"enable" then
            AbilityAlertFilterOptions.useBoth = false
            AbilityAlertFilterOptions.isWCTDisabled = false
            if WSCT then
                print("WSCT output enabled.")
            else
                print("WSCT output enabled but you do not have WSCT installed so will not function.")
            end
        elseif (val:lower()) == L"both" then
            AbilityAlertFilterOptions.useBoth = true
            if WSCT then
                print("Standard and WSCT output both enabled.")
            else
                print("Standard and WSCT output both enabled but you do not have WSCT installed.")
            end
        else
            print("Usage: /abilityalert wsct [enable|disable]")
        end
    -- set WSCT frame
    elseif opt == L"wsctframe" then
        if val == L"" or not val then
            print("Usage: /abilityalert wsctframe [1|2|3]")
        elseif (val:lower()) == L"1" then
            AbilityAlertFilterOptions.WSCTFrame = 1
            print("-WSCT frame set to 1.")
        elseif (val:lower()) == L"2" then
            AbilityAlertFilterOptions.WSCTFrame = 2
            print("-WSCT frame set to 2.")
        elseif (val:lower()) == L"3" then
            AbilityAlertFilterOptions.WSCTFrame = 3
            print("-WSCT frame set to 3.")
        else
            print("Usage: /abilityalert wsctframe [1|2|3]")
        end

    -- set show flag
    elseif opt == L"show" then
        if val == L"" or not val then
            print("Usage: /abilityalert show [all|none]")
        elseif (val:lower()) == L"on" or (val:lower()) == L"all" or (val:lower()) == L"enable"then
            AbilityAlertFilterOptions.showAll = true
            print("--showAll = ENABLED, filters now hide abilities.")
        elseif (val:lower()) == L"off" or (val:lower()) == L"disable" or (val:lower()) == L"none" then
            AbilityAlertFilterOptions.showAll = false
            print("--showAll = DISABLED, filters now display abilities.")
        else
            print("Usage: /abilityalert show [all|none]")
        end

    -- OPTION NOT FOUND
    else
        print(L"'"..opt..L"' is not a valid option for /abilityalert.")
        print(L"See '/abilityalert' for a list of options.")
    end 
end

-- scan the filters and if found return true
function AbilityAlert.isFiltered(abilityName)
        abilityName = tostring(abilityName)
        --regex exclusions
        for _,regex in ipairs(AbilityAlertFilterOptions.ExcludedAbilities) do
            regex = tostring(regex)
            if (abilityName:lower()):find(regex) then return true end
        end
    
    return false
end

-- scan the monitors and if found return true
function AbilityAlert.isReactiveMonitored(abilityName)
        if (abilityName ~= nil and AbilityAlertFilterOptions.ReactiveAbilities ~= nil) then
            abilityName = tostring(abilityName)
            
            --regex exclusions
            for _,regex in ipairs(AbilityAlertFilterOptions.ReactiveAbilities) do
                regex = tostring(regex)
                if (abilityName:lower()):find(regex) then return true end
            end
        end
    
    return false
end
